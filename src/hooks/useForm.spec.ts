import { renderHook, act } from '@testing-library/react';
import { useForm } from '@/hooks/useForm';
import { ChangeEvent, FormEvent } from 'react';

interface FormTestData {
  firstName: string;
  lastName: string;
}

describe('useForm', () => {
  const getTestSubmitEvent = () => {
    return {
      preventDefault: jest.fn(),
    } as unknown as FormEvent<HTMLFormElement>;
  };

  const getTestChangeEvent = (value: unknown) => {
    return {
      target: { value },
    } as unknown as ChangeEvent<HTMLInputElement>;
  };

  it('should initialize data', () => {
    const { result } = renderHook(() =>
      useForm<FormTestData>({
        initialValues: { firstName: 'Swen' },
      }),
    );

    expect(result.current.formData.firstName).toBe('Swen');
    expect(result.current.formData.lastName).toBeUndefined();
  });

  it('should update data', () => {
    const { result } = renderHook(() => useForm<FormTestData>({}));

    expect(result.current.formData.firstName).toBeUndefined();

    act(() => {
      result.current.handleChange('firstName')(getTestChangeEvent('Swen'));
    });

    expect(result.current.formData.firstName).toBe('Swen');
  });

  describe('when submit is triggered', () => {
    it('should not call onSubmit callback if form is invalid', () => {
      const onSubmit = jest.fn();

      const { result } = renderHook(() =>
        useForm<FormTestData>({
          initialValues: {
            firstName: 'Swen',
            lastName: '',
          },
          validations: {
            firstName: {
              required: {
                value: true,
                message: 'First name is missing',
              },
            },
            lastName: {
              required: {
                value: true,
                message: 'Last name is missing',
              },
            },
          },
          onSubmit,
        }),
      );

      act(() => {
        result.current.handleSubmit(getTestSubmitEvent());
      });

      expect(result.current.errors).toEqual({ lastName: 'Last name is missing' });
      expect(onSubmit).not.toBeCalled();
    });

    it('should call onSubmit callback if form is valid', () => {
      const onSubmit = jest.fn();

      const { result } = renderHook(() =>
        useForm<FormTestData>({
          initialValues: {
            firstName: 'Swen',
            lastName: 'Swanson',
          },
          validations: {
            firstName: {
              required: {
                value: true,
                message: 'First name is missing',
              },
            },
            lastName: {
              required: {
                value: true,
                message: 'Last name is missing',
              },
            },
          },
          onSubmit,
        }),
      );

      act(() => {
        result.current.handleSubmit(getTestSubmitEvent());
      });

      expect(result.current.errors).toEqual({});
      expect(onSubmit).toBeCalledWith({ firstName: 'Swen', lastName: 'Swanson' });
    });
  });
});
