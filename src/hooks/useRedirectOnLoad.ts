import { useNavigate } from 'react-router-dom';
import { useEffect } from 'react';

export const useRedirectOnLoad = (to: string, condition: boolean) => {
  const navigate = useNavigate();

  useEffect(() => {
    if (condition) {
      navigate(to);
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
};
