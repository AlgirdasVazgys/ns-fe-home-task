import { useState, ChangeEvent, FormEvent } from 'react';
import { FormErrors, FormValidations } from '@/types/form';
import { validateForm } from '@/utils/validateForm';

interface UseForm<T extends Record<keyof T, unknown>> {
  validations?: FormValidations<T>;
  initialValues?: Partial<T>;
  onSubmit?: (data: T) => void;
}

export const useForm = <T extends Record<keyof T, unknown>>({
  validations,
  initialValues = {},
  onSubmit,
}: UseForm<T>) => {
  const [formData, setFormData] = useState<T>(initialValues as T);
  const [errors, setErrors] = useState<FormErrors<T>>({});

  const handleChange = (key: keyof T) => (e: ChangeEvent<HTMLInputElement>) => {
    setFormData({
      ...formData,
      [key]: e.target.value,
    });
  };

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    const { isValid, errors: newErrors } = validateForm<T>({ validations, formData });

    if (!isValid) {
      setErrors(newErrors);
      return;
    }

    setErrors({});

    if (onSubmit) {
      onSubmit(formData);
    }
  };

  return { formData, handleChange, handleSubmit, errors };
};
