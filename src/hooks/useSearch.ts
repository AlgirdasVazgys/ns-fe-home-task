import { ChangeEvent, useState } from 'react';

export const useSearch = () => {
  const [searchValue, setSearchValue] = useState('');

  const handleValueChange = (e: ChangeEvent<HTMLInputElement>) => {
    setSearchValue(e.target.value.trim());
  };

  return { searchValue, handleValueChange };
};
