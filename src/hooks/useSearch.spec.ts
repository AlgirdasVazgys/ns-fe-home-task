import { ChangeEvent } from 'react';
import { renderHook, act } from '@testing-library/react';
import { useSearch } from './useSearch';

describe('useSearch', () => {
  it('should trim entered string', () => {
    const { result } = renderHook(() => useSearch());

    act(() => {
      result.current.handleValueChange({
        target: { value: 'Value ' },
      } as ChangeEvent<HTMLInputElement>);
    });

    expect(result.current.searchValue).toBe('Value');
  });
});
