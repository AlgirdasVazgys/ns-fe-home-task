import { renderHook } from '@testing-library/react';
import { useRedirectOnLoad } from './useRedirectOnLoad';

const mockedUsedNavigate = jest.fn();

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useNavigate: () => mockedUsedNavigate,
}));

describe('useRedirectOnLoad', () => {
  describe('when condition is not met on load', () => {
    it('does not redirect', () => {
      renderHook(() => useRedirectOnLoad('/login', false));
      expect(mockedUsedNavigate).not.toHaveBeenCalled();
    });

    it('does not redirect if condition changes', () => {
      let condition = false;
      const { rerender } = renderHook(() => useRedirectOnLoad('/login', condition));

      condition = true;
      rerender();

      expect(mockedUsedNavigate).not.toHaveBeenCalled();
    });
  });

  describe('when condition is met on load', () => {
    it('redirects', () => {
      renderHook(() => useRedirectOnLoad('/login', true));
      expect(mockedUsedNavigate).toHaveBeenCalledWith('/login');
    });
  });
});
