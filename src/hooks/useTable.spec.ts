import { renderHook, act } from '@testing-library/react';
import { useTable } from '@/hooks/useTable';
import { ORDER } from '@/constants/misc';

describe('useTable', () => {
  const tableData = {
    columns: [
      {
        accessor: 'firstName',
        label: 'First name',
      },
      {
        accessor: 'lastName',
        label: 'Last name',
      },
    ],
    data: [
      { firstName: 'Swen', lastName: 'Swanson' },
      { firstName: 'Josh', lastName: 'Burgundy' },
      { firstName: 'Aaron', lastName: 'Nash' },
    ],
  };

  it('should build table rows and columns and sort rows by first accessor on init', () => {
    const { result } = renderHook(() => useTable(tableData));

    expect(result.current.rows).toEqual([
      ['Aaron', 'Nash'],
      ['Josh', 'Burgundy'],
      ['Swen', 'Swanson'],
    ]);
    expect(result.current.headers).toEqual([
      {
        accessor: 'firstName',
        label: 'First name',
      },
      {
        accessor: 'lastName',
        label: 'Last name',
      },
    ]);
    expect(result.current.sortConfig).toEqual({ sortBy: 'firstName', order: ORDER.ASC });
  });

  describe('when sort by same accessor triggered', () => {
    it('should sort rows in toggled order', () => {
      const { result } = renderHook(() => useTable(tableData));

      expect(result.current.rows).toEqual([
        ['Aaron', 'Nash'],
        ['Josh', 'Burgundy'],
        ['Swen', 'Swanson'],
      ]);
      expect(result.current.sortConfig.order).toEqual(ORDER.ASC);

      act(() => {
        result.current.handleSortChange('firstName');
      });

      expect(result.current.rows).toEqual([
        ['Swen', 'Swanson'],
        ['Josh', 'Burgundy'],
        ['Aaron', 'Nash'],
      ]);
      expect(result.current.sortConfig.order).toEqual(ORDER.DESC);
    });
  });

  describe('when sort by different accessor triggered', () => {
    it('should sort rows in ascending order', () => {
      const { result } = renderHook(() => useTable(tableData));

      expect(result.current.rows).toEqual([
        ['Aaron', 'Nash'],
        ['Josh', 'Burgundy'],
        ['Swen', 'Swanson'],
      ]);
      expect(result.current.sortConfig.order).toEqual(ORDER.ASC);

      act(() => {
        result.current.handleSortChange('lastName');
      });

      expect(result.current.rows).toEqual([
        ['Josh', 'Burgundy'],
        ['Aaron', 'Nash'],
        ['Swen', 'Swanson'],
      ]);
      expect(result.current.sortConfig.order).toEqual(ORDER.ASC);
    });
  });
});
