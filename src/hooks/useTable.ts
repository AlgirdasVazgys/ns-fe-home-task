import { useMemo, useState } from 'react';
import { ORDER } from '@/constants/misc';
import { sortObjectsList } from '@/utils/sortObjectsList';

interface SortConfig {
  sortBy: string | undefined;
  order: ORDER | undefined;
}

interface UseTableProps {
  // eslint-disable-next-line  @typescript-eslint/no-explicit-any
  data: Record<string, any>[];
  columns: {
    accessor: string;
    label: string;
  }[];
}

export interface UseTableReturn {
  headers: { label: string; accessor: string }[];
  rows: (string | number)[][];
  sortConfig: SortConfig;
  handleSortChange(sortBy: string): void;
}

export const useTable = ({ data, columns }: UseTableProps): UseTableReturn => {
  const [sortConfig, setSortConfig] = useState<SortConfig>({
    sortBy: columns[0]?.accessor,
    order: ORDER.ASC,
  });

  const rows = useMemo(() => {
    if (data.length === 0) {
      return [];
    }

    let dataToDisplay = data;

    if (sortConfig.sortBy && sortConfig.order) {
      dataToDisplay = sortObjectsList({
        list: dataToDisplay,
        sortBy: sortConfig.sortBy,
        order: sortConfig.order,
      });
    }

    return dataToDisplay.map((item) => columns.map(({ accessor }) => item[accessor]));
  }, [data, columns, sortConfig]);

  function handleSortChange(sortBy: string) {
    setSortConfig({
      sortBy,
      order: getNewOrder(sortBy),
    });
  }

  function getNewOrder(newSortBy: string) {
    if (!sortConfig.order || newSortBy !== sortConfig.sortBy) {
      return ORDER.ASC;
    }

    return sortConfig.order === ORDER.ASC ? ORDER.DESC : ORDER.ASC;
  }

  return {
    headers: columns,
    rows,
    sortConfig,
    handleSortChange,
  };
};
