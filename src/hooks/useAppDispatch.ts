import { useDispatch } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';
import { RootState } from '@/store/rootReducer';

export const useAppDispatch: () => ThunkDispatch<RootState, unknown, AnyAction> = useDispatch;
