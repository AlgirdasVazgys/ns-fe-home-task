import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { HelmetProvider } from 'react-helmet-async';
import { configureStore } from '@/store/configureStore';
import { AppRouter } from '@/routes/AppRouter';
import { ErrorBoundary } from '@/components/misc/ErrorBoundary';
import { setupInterceptors } from '@/services/setupInterceptors';
import { initialRootState } from '@/store/initialRootState';

const store = configureStore(initialRootState);

export const App = () => {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <HelmetProvider>
          <ErrorBoundary>
            <AppRouter />
          </ErrorBoundary>
        </HelmetProvider>
      </BrowserRouter>
    </Provider>
  );
};

setupInterceptors(store);
