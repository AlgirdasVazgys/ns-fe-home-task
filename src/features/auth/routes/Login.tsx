import { useAppSelector } from '@/hooks/useAppSelector';
import { useRedirectOnLoad } from '@/hooks/useRedirectOnLoad';
import { ROUTE } from '@/constants/routes';
import { LoginForm } from '../components/LoginForm';
import { LoginLayout } from '../components/LoginLayout';

export const Login = () => {
  const { isAuthenticated } = useAppSelector((state) => state.auth);
  useRedirectOnLoad(`/${ROUTE.SERVERS}`, isAuthenticated);

  return (
    <LoginLayout>
      <LoginForm />
    </LoginLayout>
  );
};
