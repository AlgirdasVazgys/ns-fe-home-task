import mockAxios from 'jest-mock-axios';
import { AUTH, login, logout } from '@/features/auth';
import { storage } from '@/services/storage';
import { ERROR } from '@/constants/misc';

describe('Auth thunk action', () => {
  describe('login', () => {
    describe('when api call is successful', () => {
      it('should save token in storage, dispatch success action and return true', async () => {
        const credentials = { username: 'username', password: 'password' };
        const dispatch = jest.fn();
        jest.spyOn(storage, 'setItem');
        mockAxios.post.mockResolvedValueOnce({ data: { token: 'Token' } });

        const thunk = login(credentials);
        const thunkPromise = thunk(dispatch, jest.fn(), null);

        const result = await thunkPromise;

        expect(dispatch).toHaveBeenCalledWith({ type: AUTH.LOGIN_STARTED });
        expect(dispatch).toHaveBeenCalledWith({ type: AUTH.LOGIN_SUCCESS });
        expect(result).toBe(true);
        expect(storage.setItem).toHaveBeenCalledWith('ns_servers_token', 'Token');
      });
    });

    describe('when api call fails', () => {
      it('should dispatch action with default error and return false', async () => {
        const credentials = { username: 'username', password: 'password' };
        const dispatch = jest.fn();
        jest.spyOn(storage, 'setItem').mockImplementation(jest.fn());
        mockAxios.post.mockRejectedValueOnce({ response: { status: 499 } });

        const thunk = login(credentials);
        const thunkPromise = thunk(dispatch, jest.fn(), null);

        const result = await thunkPromise;

        expect(dispatch).toHaveBeenCalledWith({ type: AUTH.LOGIN_STARTED });
        expect(dispatch).toHaveBeenCalledWith({ type: AUTH.LOGIN_FAILED, payload: ERROR.DEFAULT });
        expect(result).toBe(false);
      });

      it('should dispatch action with login error and return false', async () => {
        const credentials = { username: 'username', password: 'password' };
        const dispatch = jest.fn();
        mockAxios.post.mockRejectedValueOnce({ response: { status: 401 } });

        const thunk = login(credentials);
        const thunkPromise = thunk(dispatch, jest.fn(), null);

        const result = await thunkPromise;

        expect(dispatch).toHaveBeenCalledWith({ type: AUTH.LOGIN_STARTED });
        expect(dispatch).toHaveBeenCalledWith({ type: AUTH.LOGIN_FAILED, payload: ERROR.LOGIN });
        expect(result).toBe(false);
      });
    });
  });

  describe('logout', () => {
    it('should remove token from storage and dispatch logout action', () => {
      const dispatch = jest.fn();
      jest.spyOn(storage, 'removeItem').mockImplementation(jest.fn());

      const thunk = logout();

      thunk(dispatch, jest.fn(), null);

      expect(dispatch).toHaveBeenCalledWith({ type: AUTH.LOGOUT });
      expect(storage.removeItem).toHaveBeenCalledWith('ns_servers_token');
    });
  });
});
