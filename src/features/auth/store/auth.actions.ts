import { AppThunk } from '@/store/rootReducer';
import { storage } from '@/services/storage';
import { TOKEN_NAME } from '@/constants/metadata';
import { ERROR } from '@/constants/misc';
import { getToken } from '../api/getToken';

export enum AUTH {
  LOGIN_STARTED = 'auth/loginStarted',
  LOGIN_SUCCESS = 'auth/loginSuccess',
  LOGIN_FAILED = 'auth/loginFailed',
  LOGOUT = 'auth/logout',
}

export const login =
  ({ username, password }: { username: string; password: string }): AppThunk<Promise<boolean>> =>
  (dispatch) => {
    dispatch({ type: AUTH.LOGIN_STARTED });
    return getToken({ username, password })
      .then(({ data }) => {
        storage.setItem(TOKEN_NAME, data.token);
        dispatch({ type: AUTH.LOGIN_SUCCESS });
        return true;
      })
      .catch((error) => {
        const message = error.response?.status === 401 ? ERROR.LOGIN : ERROR.DEFAULT;
        dispatch({ type: AUTH.LOGIN_FAILED, payload: message });
        return false;
      });
  };

export const logout = (): AppThunk => (dispatch) => {
  dispatch({ type: AUTH.LOGOUT });
  storage.removeItem(TOKEN_NAME);
};
