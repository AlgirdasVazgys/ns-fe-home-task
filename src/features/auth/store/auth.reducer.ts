import { AnyAction } from 'redux';
import { AUTH } from './auth.actions';

interface AuthState {
  isLoading: boolean;
  isAuthenticated: boolean;
  error: string;
}

export const initialState = {
  isLoading: false,
  isAuthenticated: false,
  error: '',
};

export const authReducer = (state: AuthState = initialState, action: AnyAction): AuthState => {
  switch (action.type) {
    case AUTH.LOGIN_STARTED:
      return {
        ...state,
        isLoading: true,
        error: '',
      };
    case AUTH.LOGIN_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isAuthenticated: true,
      };
    case AUTH.LOGIN_FAILED:
      return {
        ...state,
        isLoading: false,
        error: action.payload,
      };
    case AUTH.LOGOUT:
      return {
        ...state,
        isAuthenticated: false,
      };
    default:
      return state;
  }
};
