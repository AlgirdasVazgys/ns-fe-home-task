import { AUTH, authReducer } from '@/features/auth';

export const initialState = {
  isLoading: false,
  isAuthenticated: false,
  error: '',
};

describe('Auth reducer', () => {
  it('should return the initial state', () => {
    expect(authReducer(undefined, { type: undefined })).toEqual(initialState);
  });

  it('should handle login start', () => {
    expect(authReducer(initialState, { type: AUTH.LOGIN_STARTED })).toEqual({
      ...initialState,
      isLoading: true,
    });
  });

  it('should handle login success', () => {
    expect(
      authReducer(
        {
          ...initialState,
          isLoading: true,
        },
        { type: AUTH.LOGIN_SUCCESS },
      ),
    ).toEqual({
      ...initialState,
      isLoading: false,
      isAuthenticated: true,
    });
  });

  it('should handle login failure', () => {
    expect(
      authReducer(
        {
          ...initialState,
          isLoading: true,
        },
        { type: AUTH.LOGIN_FAILED, payload: 'Error' },
      ),
    ).toEqual({
      ...initialState,
      isLoading: false,
      error: 'Error',
    });
  });

  it('should handle logout', () => {
    expect(
      authReducer(
        {
          ...initialState,
          isAuthenticated: true,
        },
        { type: AUTH.LOGOUT },
      ),
    ).toEqual({
      ...initialState,
      isAuthenticated: false,
    });
  });
});
