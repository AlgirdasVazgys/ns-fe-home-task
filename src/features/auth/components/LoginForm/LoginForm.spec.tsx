import mockAxios from 'jest-mock-axios';
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { renderWithProviders } from '@/test/testutils';
import { ERROR } from '@/constants/misc';
import { LoginForm } from './LoginForm';

const mockedUsedNavigate = jest.fn();

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useNavigate: () => mockedUsedNavigate,
}));

describe('LoginForm component', () => {
  describe('submitting login', () => {
    describe('when username or password is missing', () => {
      it('should render form errors', async () => {
        const user = userEvent.setup();
        renderWithProviders(<LoginForm />);

        const usernameField = screen.getByLabelText('Username');
        const submitButton = screen.getByTestId('LoginButton');

        await user.type(usernameField, 'johnny212');
        await user.click(submitButton);

        expect(screen.getByText(ERROR.REQUIRED_PASSWORD)).toBeInTheDocument();
      });
    });

    describe('when api request failure', () => {
      it('should render server error', async () => {
        mockAxios.post.mockRejectedValueOnce({ response: { status: 401 } });
        const user = userEvent.setup();
        renderWithProviders(<LoginForm />);

        const usernameField = screen.getByLabelText('Username');
        const passwordField = screen.getByLabelText('Password');
        const submitButton = screen.getByTestId('LoginButton');

        await user.type(usernameField, 'johnny212');
        await user.type(passwordField, 'classic1234656');
        await user.click(submitButton);

        expect(screen.getByText(ERROR.LOGIN)).toBeInTheDocument();
        expect(mockedUsedNavigate).not.toHaveBeenCalled();
      });
    });

    describe('when api request success', () => {
      it('should navigate to servers page', async () => {
        mockAxios.post.mockResolvedValueOnce({ data: { token: 'Token' } });
        const user = userEvent.setup();
        renderWithProviders(<LoginForm />);

        const usernameField = screen.getByLabelText('Username');
        const passwordField = screen.getByLabelText('Password');
        const submitButton = screen.getByTestId('LoginButton');

        await user.type(usernameField, 'johnny212');
        await user.type(passwordField, 'classic1234656');
        await user.click(submitButton);

        expect(mockedUsedNavigate).toHaveBeenCalledWith('/servers');
      });
    });
  });
});
