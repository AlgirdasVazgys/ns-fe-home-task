import { useNavigate } from 'react-router-dom';
import { login } from '@/features/auth/store/auth.actions';
import { useAppDispatch } from '@/hooks/useAppDispatch';
import { useAppSelector } from '@/hooks/useAppSelector';
import { ROUTE } from '@/constants/routes';
import { ERROR } from '@/constants/misc';
import { FormValidations } from '@/types/form';
import { useForm } from '@/hooks/useForm';
import { FormTitle } from '@/components/form/FormTitle';
import { Form } from '@/components/form/Form';
import { FormError } from '@/components/form/FormError';
import { FormField } from '@/components/form/FormField';
import { Button } from '@/components/ui/Button';
import { UserCredentials } from '../../types';

import './LoginForm.scss';

const loginFormValidations: FormValidations<UserCredentials> = {
  username: {
    required: {
      value: true,
      message: ERROR.REQUIRED_USERNAME,
    },
  },
  password: {
    required: {
      value: true,
      message: ERROR.REQUIRED_PASSWORD,
    },
  },
};

export const LoginForm = () => {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const { error: loginError, isLoading } = useAppSelector((state) => state.auth);
  const {
    handleSubmit,
    handleChange,
    formData,
    errors: validationErrors,
  } = useForm<UserCredentials>({
    validations: loginFormValidations,
    initialValues: {
      username: '',
      password: '',
    },
    onSubmit: ({ username, password }) => {
      dispatch(login({ username, password })).then((isAuthenticated) => {
        if (isAuthenticated) {
          navigate(`/${ROUTE.SERVERS}`);
        }
      });
    },
  });

  return (
    <div className="LoginForm">
      <Form handleSubmit={handleSubmit}>
        <FormTitle title="Login" />
        {loginError && <FormError text={loginError} />}
        <FormField
          label="Username"
          type="text"
          error={validationErrors.username}
          name="username"
          onChange={handleChange('username')}
          value={formData.username}
        />
        <FormField
          label="Password"
          type="password"
          error={validationErrors.password}
          name="password"
          onChange={handleChange('password')}
          value={formData.password}
        />
        <Button data-testid="LoginButton" full disabled={isLoading} type="submit">
          {isLoading ? 'Logging in...' : 'Login'}
        </Button>
      </Form>
    </div>
  );
};
