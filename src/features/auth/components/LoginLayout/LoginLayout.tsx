import { FC, PropsWithChildren, ReactNode } from 'react';
import { Head } from '@/components/misc/Head';
import { BaseLayout } from '@/components/layout/BaseLayout';

interface LoginLayoutProps {
  children?: ReactNode;
}

export const LoginLayout: FC<PropsWithChildren<LoginLayoutProps>> = ({ children }) => {
  return (
    <BaseLayout>
      <Head title="Login" />
      {children}
    </BaseLayout>
  );
};
