import { AxiosResponse } from 'axios';
import { apiClient } from '@/services/apiClient';
import { API_ENDPOINT } from '@/constants/api';
import { UserCredentials } from '../types';

export const getToken = ({
  username,
  password,
}: UserCredentials): Promise<AxiosResponse<{ token: string }>> =>
  apiClient.post(API_ENDPOINT.TOKENS, { username, password });
