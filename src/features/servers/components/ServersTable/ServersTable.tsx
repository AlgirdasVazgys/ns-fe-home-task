import { useMemo } from 'react';
import { Table } from '@/components/table/Table';
import { SearchInput } from '@/components/ui/SearchInput';
import { Loader } from '@/components/ui/Loader';
import { useTable } from '@/hooks/useTable';
import { useAppSelector } from '@/hooks/useAppSelector';
import { useSearch } from '@/hooks/useSearch';
import { ServerTableColumn } from '../../types';

import './ServersTable.scss';

const SERVERS_TABLE_COLUMNS: ServerTableColumn[] = [
  {
    accessor: 'name',
    label: 'Name',
  },
  {
    accessor: 'distance',
    label: 'Distance',
  },
];

export const ServersTable = () => {
  const { servers, isLoading, error } = useAppSelector((state) => state.servers);
  const { searchValue, handleValueChange } = useSearch();
  const { rows, ...tableProps } = useTable({ data: servers, columns: SERVERS_TABLE_COLUMNS });

  const filteredRows = useMemo(() => {
    return rows.filter((cells) => {
      return cells.some((cell) => `${cell}`.toLowerCase().includes(searchValue.toLowerCase()));
    });
  }, [searchValue, rows]);

  const shouldDisplayError = error && servers.length === 0;

  return (
    <div className="ServersTable">
      <SearchInput
        className="ServersTable__search-input"
        onChange={handleValueChange}
        value={searchValue}
      />
      {isLoading ? (
        <Loader />
      ) : (
        <>
          {shouldDisplayError ? (
            <div className="ServersTable__message">
              <span>{error}</span>
            </div>
          ) : (
            <Table rows={filteredRows} {...tableProps} />
          )}
        </>
      )}
    </div>
  );
};
