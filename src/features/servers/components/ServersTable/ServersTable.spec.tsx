import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { renderWithProviders } from '@/test/testutils';
import { initialState } from '@/features/servers';
import { ServersTable } from './ServersTable';

describe('ServersTable component', () => {
  const testServers = [
    { name: 'Austin', distance: 8000 },
    { name: 'Brussels', distance: 2000 },
    { name: 'Vienna', distance: 1000 },
  ];

  describe('when servers data is loading', () => {
    it('should render loader and hide table', () => {
      renderWithProviders(<ServersTable />, {
        preloadedState: {
          servers: {
            ...initialState,
            isLoading: true,
          },
        },
      });

      const loader = screen.getByTestId('Loader');
      const table = screen.queryByRole('table');

      expect(loader).toBeInTheDocument();
      expect(table).not.toBeInTheDocument();
    });
  });

  describe('when servers data load finishes with success', () => {
    it('should render table', () => {
      renderWithProviders(<ServersTable />, {
        preloadedState: {
          servers: {
            ...initialState,
            isLoading: false,
            servers: testServers,
          },
        },
      });

      const table = screen.getByRole('table');
      const tableHeadCell = screen.getByRole('columnheader', { name: /name/i });
      const tableCell = screen.getByRole('cell', { name: /Austin/i });

      expect(table).toBeInTheDocument();
      expect(tableHeadCell).toBeInTheDocument();
      expect(tableCell).toBeInTheDocument();
    });
  });

  describe('when servers data load finishes with error', () => {
    it('should render error and hide table if empty servers list', () => {
      renderWithProviders(<ServersTable />, {
        preloadedState: {
          servers: {
            ...initialState,
            isLoading: false,
            error: 'Server error',
            servers: [],
          },
        },
      });

      const table = screen.queryByRole('table');
      const error = screen.getByText('Server error');

      expect(table).not.toBeInTheDocument();
      expect(error).toBeInTheDocument();
    });

    it('should hide error and render table if servers list not empty', () => {
      renderWithProviders(<ServersTable />, {
        preloadedState: {
          servers: {
            ...initialState,
            isLoading: false,
            error: 'Server error',
            servers: testServers,
          },
        },
      });

      const table = screen.getByRole('table');
      const error = screen.queryByText('Server error');

      expect(table).toBeInTheDocument();
      expect(error).not.toBeInTheDocument();
    });
  });

  describe('when searching', () => {
    it('should filter table', async () => {
      const user = userEvent.setup();
      renderWithProviders(<ServersTable />, {
        preloadedState: {
          servers: {
            ...initialState,
            isLoading: false,
            servers: testServers,
          },
        },
      });

      expect(screen.getByRole('cell', { name: /Austin/i })).toBeInTheDocument();
      expect(screen.getByRole('cell', { name: /Brussels/i })).toBeInTheDocument();

      const search = screen.getByPlaceholderText('Search...');
      await user.type(search, 'Brus');

      expect(screen.queryByRole('cell', { name: /Austin/i })).not.toBeInTheDocument();
      expect(screen.getByRole('cell', { name: /Brussels/i })).toBeInTheDocument();
    });
  });
});
