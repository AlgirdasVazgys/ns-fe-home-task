import { AxiosResponse } from 'axios';
import { apiClient } from '@/services/apiClient';
import { API_ENDPOINT } from '@/constants/api';
import { ServerInfo } from '@/features/servers/types';

export const getServers = (): Promise<AxiosResponse<ServerInfo[]>> =>
  apiClient.get(API_ENDPOINT.SERVERS);
