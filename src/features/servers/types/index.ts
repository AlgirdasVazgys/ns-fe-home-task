export interface ServerInfo {
  name: string;
  distance: number;
}

export interface ServerTableColumn {
  accessor: keyof ServerInfo;
  label: string;
}
