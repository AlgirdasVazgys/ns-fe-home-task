import mockAxios from 'jest-mock-axios';
import { loadServers, SERVERS } from './servers.actions';

describe('Servers thunk action', () => {
  afterEach(() => {
    mockAxios.reset();
  });

  describe('loadServers', () => {
    describe('when api call is successful', () => {
      it('should dispatch action with servers', async () => {
        const servers = [{ name: 'Server', distance: 100 }];
        const dispatch = jest.fn();
        mockAxios.get.mockResolvedValueOnce({ data: servers });

        const thunk = loadServers();
        const thunkPromise = thunk(dispatch, jest.fn(), null);

        await thunkPromise;

        expect(dispatch).toHaveBeenCalledWith({ type: SERVERS.LOAD_STARTED });
        expect(dispatch).toHaveBeenCalledWith({ type: SERVERS.LOAD_SUCCESS, payload: servers });
      });
    });

    describe('when api call fails', () => {
      it('should dispatch action with error', async () => {
        const dispatch = jest.fn();
        mockAxios.get.mockRejectedValueOnce(null);

        const thunk = loadServers();
        const thunkPromise = thunk(dispatch, jest.fn(), null);

        await thunkPromise;

        expect(dispatch).toHaveBeenCalledWith({ type: SERVERS.LOAD_STARTED });
        expect(dispatch).toHaveBeenCalledWith({
          type: SERVERS.LOAD_FAILED,
          payload: expect.any(String),
        });
      });
    });
  });
});
