import { AppThunk } from '@/store/rootReducer';
import { ERROR } from '@/constants/misc';
import { getServers } from '../api/getServers';

export enum SERVERS {
  LOAD_STARTED = 'servers/loadStarted',
  LOAD_SUCCESS = 'servers/loadSuccess',
  LOAD_FAILED = 'server/loadFailed',
}

export const loadServers = (): AppThunk<Promise<void>> => (dispatch) => {
  dispatch({ type: SERVERS.LOAD_STARTED });
  return getServers()
    .then(({ data }) => {
      dispatch({ type: SERVERS.LOAD_SUCCESS, payload: data });
    })
    .catch(() => {
      dispatch({ type: SERVERS.LOAD_FAILED, payload: ERROR.DEFAULT });
    });
};
