import { serversReducer } from '@/features/servers';
import { SERVERS } from '@/features/servers/store/servers.actions';
import { ServerInfo } from '@/features/servers/types';

const initialState = {
  servers: [],
  isLoading: false,
  error: '',
};

describe('Servers reducer', () => {
  it('should return the initial state', () => {
    expect(serversReducer(undefined, { type: undefined })).toEqual(initialState);
  });

  it('should handle servers load start', () => {
    expect(serversReducer(initialState, { type: SERVERS.LOAD_STARTED })).toEqual({
      ...initialState,
      isLoading: true,
    });
  });

  it('should handle servers load success', () => {
    const servers: ServerInfo[] = [{ name: 'Server', distance: 100 }];

    expect(
      serversReducer(
        {
          ...initialState,
          isLoading: true,
        },
        { type: SERVERS.LOAD_SUCCESS, payload: servers },
      ),
    ).toEqual({
      ...initialState,
      isLoading: false,
      servers,
    });
  });

  it('should handle servers load failure', () => {
    const servers: ServerInfo[] = [{ name: 'Server', distance: 100 }];

    expect(
      serversReducer(
        {
          ...initialState,
          servers,
          isLoading: true,
        },
        { type: SERVERS.LOAD_FAILED, payload: 'Error' },
      ),
    ).toEqual({
      ...initialState,
      servers,
      isLoading: false,
      error: 'Error',
    });
  });
});
