import { AnyAction } from 'redux';
import { SERVERS } from './servers.actions';
import { ServerInfo } from '../types';

interface ServersState {
  servers: ServerInfo[];
  isLoading: boolean;
  error: string;
}

export const initialState = {
  servers: [],
  isLoading: false,
  error: '',
};

export const serversReducer = (
  state: ServersState = initialState,
  action: AnyAction,
): ServersState => {
  switch (action.type) {
    case SERVERS.LOAD_STARTED:
      return {
        ...state,
        isLoading: true,
      };
    case SERVERS.LOAD_SUCCESS:
      return {
        ...state,
        isLoading: false,
        servers: action.payload,
      };
    case SERVERS.LOAD_FAILED:
      return {
        ...state,
        isLoading: false,
        error: action.payload,
      };
    default:
      return state;
  }
};
