import { useEffect } from 'react';
import { useAppDispatch } from '@/hooks/useAppDispatch';
import { Head } from '@/components/misc/Head';
import { ServersTable } from '../components/ServersTable';
import { loadServers } from '../store/servers.actions';

export const Servers = () => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(loadServers());
  }, [dispatch]);

  return (
    <>
      <Head title="Servers" />
      <ServersTable />
    </>
  );
};
