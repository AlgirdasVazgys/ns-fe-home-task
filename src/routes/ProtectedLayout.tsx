import { FC } from 'react';
import { Outlet, Navigate } from 'react-router-dom';
import { MainLayout } from '@/components/layout/MainLayout';
import { useAppSelector } from '@/hooks/useAppSelector';

interface ProtectedLayoutProps {
  redirectTo: string;
}

export const ProtectedLayout: FC<ProtectedLayoutProps> = ({ redirectTo }) => {
  const { isAuthenticated } = useAppSelector((state) => state.auth);

  return <MainLayout>{isAuthenticated ? <Outlet /> : <Navigate to={redirectTo} />}</MainLayout>;
};
