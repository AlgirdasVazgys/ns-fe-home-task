import { Navigate, Route, Routes } from 'react-router-dom';
import { Servers } from '@/features/servers';
import { Login } from '@/features/auth';
import { ROUTE } from '@/constants/routes';
import { ProtectedLayout } from '@/routes/ProtectedLayout';
import { useAppSelector } from '@/hooks/useAppSelector';

export const AppRouter = () => {
  const { isAuthenticated } = useAppSelector((state) => state.auth);

  return (
    <Routes>
      <Route path={ROUTE.LOGIN} element={<Login />} />
      <Route
        path="/"
        element={<Navigate replace to={isAuthenticated ? ROUTE.SERVERS : ROUTE.LOGIN} />}
      />
      <Route path="*" element={<div>Not found</div>} />

      <Route element={<ProtectedLayout redirectTo={ROUTE.LOGIN} />}>
        <Route path={ROUTE.SERVERS} element={<Servers />} />
      </Route>
    </Routes>
  );
};
