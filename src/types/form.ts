export interface Validation {
  required?: {
    value: boolean;
    message: string;
  };
}
export type FormErrors<T extends Record<keyof T, unknown>> = Partial<Record<keyof T, string>>;
export type FormValidations<T extends Record<keyof T, unknown>> = Partial<
  Record<keyof T, Validation>
>;
