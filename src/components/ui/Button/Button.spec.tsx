import { render, screen } from '@testing-library/react';
import { Button } from './Button';

describe('Button component', () => {
  it('should render value', () => {
    const testValue = 'Test value';
    render(<Button>{testValue}</Button>);
    const button = screen.getByRole('button');

    expect(button).toHaveTextContent(testValue);
  });

  it('should include "full" className', () => {
    render(<Button full />);
    const button = screen.getByRole('button');

    expect(button).toHaveClass('full');
  });
});
