import { ComponentPropsWithoutRef, FC } from 'react';
import classNames from 'classnames';

import './Button.scss';

export interface ButtonProps extends Omit<ComponentPropsWithoutRef<'button'>, 'className'> {
  full?: boolean;
}

export const Button: FC<ButtonProps> = ({ children, full, ...restProps }) => {
  return (
    <button className={classNames('Button', { full })} {...restProps}>
      {children}
    </button>
  );
};
