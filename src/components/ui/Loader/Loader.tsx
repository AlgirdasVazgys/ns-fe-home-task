import './Loader.scss';

export const Loader = () => {
  return (
    <div data-testid="Loader" className="Loader">
      <div className="Loader__spinner"></div>
    </div>
  );
};
