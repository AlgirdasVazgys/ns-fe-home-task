import { render, screen } from '@testing-library/react';
import { Loader } from '../Loader';

describe('Loader component', () => {
  it('should render', () => {
    render(<Loader />);
    const loader = screen.getByTestId('Loader');

    expect(loader).toBeInTheDocument();
  });
});
