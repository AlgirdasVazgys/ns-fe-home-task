import { ComponentPropsWithoutRef, FC } from 'react';
import classNames from 'classnames';

import './Input.scss';

export const Input: FC<ComponentPropsWithoutRef<'input'>> = ({ className, ...restProps }) => {
  return <input className={classNames('Input', className)} {...restProps} />;
};
