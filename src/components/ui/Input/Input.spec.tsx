import { render, screen } from '@testing-library/react';
import { Input } from '../Input';

describe('Input component', () => {
  it('should render value', () => {
    render(<Input value="test-value" readOnly />);
    const input = screen.getByDisplayValue('test-value');

    expect(input).toBeInTheDocument();
  });

  it('should include custom className', () => {
    render(<Input value="test-value" readOnly className="CustomClassName" />);
    const input = screen.getByDisplayValue('test-value');

    expect(input).toHaveClass('CustomClassName');
  });
});
