import { render, screen } from '@testing-library/react';
import { SearchInput } from './SearchInput';

describe('SearchInput component', () => {
  it('should render value', () => {
    render(<SearchInput value="test-value" onChange={jest.fn()} />);
    const input = screen.getByDisplayValue('test-value');

    expect(input).toBeInTheDocument();
  });

  it('should render a default placeholder', () => {
    render(<SearchInput value="" onChange={jest.fn()} />);
    const input = screen.getByPlaceholderText('Search...');

    expect(input).toBeInTheDocument();
  });

  it('should include custom className in input wrapper', () => {
    render(<SearchInput className="CustomClassName" value="test-value" onChange={jest.fn()} />);
    const input = screen.getByDisplayValue('test-value');

    expect(input).parentElement?.toHaveClass('CustomClassName');
  });
});
