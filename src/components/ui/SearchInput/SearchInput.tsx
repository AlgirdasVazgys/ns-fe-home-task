import { ChangeEvent, FC } from 'react';
import classNames from 'classnames';
import { Input } from '../Input';

import './SearchInput.scss';

interface SearchInput {
  value: string;
  className?: string;
  onChange(e: ChangeEvent<HTMLInputElement>): void;
}

export const SearchInput: FC<SearchInput> = ({ value, className, onChange }) => {
  return (
    <div className={classNames('SearchInput', className)}>
      <Input placeholder="Search..." value={value} onChange={onChange} />
    </div>
  );
};
