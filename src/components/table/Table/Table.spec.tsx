import { render, screen } from '@testing-library/react';
import { Table } from './Table';

describe('Table component', () => {
  it('should render headers and rows', () => {
    render(
      <Table
        sortConfig={{ sortBy: undefined, order: undefined }}
        handleSortChange={jest.fn()}
        rows={[['Prague'], ['Riga']]}
        headers={[{ label: 'City', accessor: 'city' }]}
      />,
    );
    const tableHeadCell = screen.getByRole('columnheader', { name: /City/i });
    const tableCell = screen.getByRole('cell', { name: /Prague/i });

    expect(tableHeadCell).toBeInTheDocument();
    expect(tableCell).toBeInTheDocument();
  });

  it('should render message when empty', () => {
    const message = 'No data to display';
    render(
      <Table
        sortConfig={{ sortBy: undefined, order: undefined }}
        handleSortChange={jest.fn()}
        rows={[]}
        headers={[{ label: 'Test label', accessor: 'test-accessor' }]}
      />,
    );
    const element = screen.getByText(message);

    expect(element).toBeInTheDocument();
  });
});
