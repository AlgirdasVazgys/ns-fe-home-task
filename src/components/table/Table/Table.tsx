import { FC } from 'react';
import { TableSortIndicator } from '../TableSortIndicator';
import { UseTableReturn } from '@/hooks/useTable';

import './Table.scss';

type TableProps = UseTableReturn;

export const Table: FC<TableProps> = ({ headers, rows, sortConfig, handleSortChange }) => {
  return (
    <div className="Table">
      <table role="table">
        <thead role="rowgroup">
          <tr role="row">
            {headers.map(({ label, accessor }) => (
              <th role="columnheader" key={accessor}>
                <button className="Table__sort-button" onClick={() => handleSortChange(accessor)}>
                  <span>{label}</span>
                  <TableSortIndicator
                    order={sortConfig.sortBy === accessor ? sortConfig.order : undefined}
                  />
                </button>
              </th>
            ))}
          </tr>
        </thead>
        <tbody role="rowgroup">
          {rows.length === 0 ? (
            <tr role="row">
              <td role="cell" colSpan={0}>
                No data to display
              </td>
            </tr>
          ) : (
            rows.map((cells, i) => (
              <tr role="row" key={i}>
                {cells.map((cell, j) => (
                  <td role="cell" key={j}>
                    {cell}
                  </td>
                ))}
              </tr>
            ))
          )}
        </tbody>
      </table>
    </div>
  );
};
