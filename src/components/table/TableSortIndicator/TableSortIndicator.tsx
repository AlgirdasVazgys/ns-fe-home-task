import { FC, useMemo } from 'react';
import { ORDER } from '@/constants/misc';
import SortIcon from '@/assets/icons/sort.svg';
import SortUpIcon from '@/assets/icons/sort-up.svg';
import SortDownIcon from '@/assets/icons/sort-down.svg';

import './TableSortIndicator.scss';

interface TableSortIndicatorProps {
  order: ORDER | undefined;
}

export const TableSortIndicator: FC<TableSortIndicatorProps> = ({ order }) => {
  const iconSrc = useMemo(() => {
    if (!order) {
      return SortIcon;
    }

    return order === ORDER.ASC ? SortUpIcon : SortDownIcon;
  }, [order]);
  return <img className="TableSortIndicator" src={iconSrc} alt="Sort" />;
};
