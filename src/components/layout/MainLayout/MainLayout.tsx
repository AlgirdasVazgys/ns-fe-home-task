import { FC, ReactNode } from 'react';
import { BaseLayout } from '../BaseLayout';
import { PageHeader } from '../PageHeader';

import './MainLayout.scss';

export interface MainLayoutProps {
  children?: ReactNode;
}

export const MainLayout: FC<MainLayoutProps> = ({ children }) => {
  return (
    <BaseLayout className="MainLayout">
      <PageHeader />
      <div className="MainLayout__content-wrapper">{children}</div>
    </BaseLayout>
  );
};
