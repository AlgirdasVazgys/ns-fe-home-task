import { screen } from '@testing-library/react';
import { renderWithProviders } from '@/test/testutils';
import { MainLayout } from './MainLayout';

describe('MainLayout component', () => {
  it('should render anything inside it', () => {
    renderWithProviders(
      <MainLayout>
        <span>test-value</span>
      </MainLayout>,
    );
    const element = screen.getByText('test-value');

    expect(element).toBeInTheDocument();
  });
});
