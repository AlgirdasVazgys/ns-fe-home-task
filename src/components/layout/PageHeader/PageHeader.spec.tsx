import userEvent from '@testing-library/user-event';
import { screen } from '@testing-library/react';
import { renderWithProviders } from '@/test/testutils';
import * as authActions from '@/features/auth/store/auth.actions';
import { PageHeader } from './PageHeader';

describe('PageHeader component', () => {
  it('should dispatch logout action on logout click', async () => {
    jest.spyOn(authActions, 'logout');
    const user = userEvent.setup();
    renderWithProviders(<PageHeader />);

    await user.click(screen.getByText('Logout'));

    expect(authActions.logout).toBeCalledTimes(1);
  });
});
