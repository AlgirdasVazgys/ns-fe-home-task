import { APP_NAME } from '@/constants/metadata';
import LogoutIcon from '@/assets/icons/logout.svg';
import { useAppDispatch } from '@/hooks/useAppDispatch';
import { logout } from '@/features/auth';

import './PageHeader.scss';

export const PageHeader = () => {
  const dispatch = useAppDispatch();
  const handleLogout = () => dispatch(logout());

  return (
    <header className="PageHeader">
      <span className="PageHeader__title">{APP_NAME}</span>
      <button className="PageHeader__logout-button" onClick={handleLogout}>
        <img src={LogoutIcon} alt="Logout" /> Logout
      </button>
    </header>
  );
};
