import { FC, ReactNode } from 'react';
import classNames from 'classnames';

import './BaseLayout.scss';

export interface BaseLayoutProps {
  children?: ReactNode;
  className?: string;
}

export const BaseLayout: FC<BaseLayoutProps> = ({ children, className }) => {
  return (
    <div className={classNames('BaseLayout', className)}>
      <div className="BaseLayout__content">{children}</div>
    </div>
  );
};
