import { render, screen } from '@testing-library/react';
import { BaseLayout } from './BaseLayout';

describe('BaseLayout component', () => {
  it('should render anything inside it', () => {
    render(
      <BaseLayout>
        <span>test-value</span>
      </BaseLayout>,
    );
    const element = screen.getByText('test-value');

    expect(element).toBeInTheDocument();
  });
});
