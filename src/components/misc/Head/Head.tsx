import { FC } from 'react';
import { Helmet } from 'react-helmet-async';
import { APP_NAME } from '@/constants/metadata';

interface HeadProps {
  title?: string;
  description?: string;
}

export const Head: FC<HeadProps> = ({ title, description = '' }) => {
  const defaultTitle = APP_NAME;

  return (
    <Helmet>
      <meta charSet="utf-8" />
      <meta name={description} />
      <title>{title ? `${title} | ${defaultTitle}` : defaultTitle}</title>
    </Helmet>
  );
};
