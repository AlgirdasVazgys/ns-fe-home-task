import userEvent from '@testing-library/user-event';
import { render, screen } from '@testing-library/react';
import { Form } from './Form';

describe('Form component', () => {
  it('should call onSubmit callback on form submit', async () => {
    const user = userEvent.setup();
    const onSubmit = jest.fn((e) => e.preventDefault());
    const submitButtonText = 'Submit';
    render(
      <Form handleSubmit={onSubmit}>
        <button type="submit">{submitButtonText}</button>
      </Form>,
    );

    await user.click(screen.getByText(submitButtonText));

    expect(onSubmit).toBeCalledTimes(1);
  });
});
