import { FC, FormEvent, ReactNode } from 'react';

import './Form.scss';

interface FormProps {
  children?: ReactNode;
  handleSubmit(e: FormEvent<HTMLFormElement>): void;
}

export const Form: FC<FormProps> = ({ children, handleSubmit }) => {
  return (
    <form className="Form" onSubmit={handleSubmit}>
      {children}
    </form>
  );
};
