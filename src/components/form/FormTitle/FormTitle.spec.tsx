import { render, screen } from '@testing-library/react';
import { FormTitle } from './FormTitle';

describe('FormTitle component', () => {
  it('should render value', () => {
    const testValue = 'Test value';
    render(<FormTitle title={testValue} />);
    const element = screen.getByText(testValue);

    expect(element).toBeInTheDocument();
  });
});
