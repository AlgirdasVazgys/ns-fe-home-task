import { FC } from 'react';

import './FormTitle.scss';

interface FormTitle {
  title: string;
}

export const FormTitle: FC<FormTitle> = ({ title }) => {
  return <h2 className="FormTitle">{title}</h2>;
};
