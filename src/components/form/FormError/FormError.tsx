import { FC } from 'react';

import './FormError.scss';

interface FormErrorProps {
  text: string;
}

export const FormError: FC<FormErrorProps> = ({ text }) => {
  return (
    <div className="FormError">
      <span>{text}</span>
    </div>
  );
};
