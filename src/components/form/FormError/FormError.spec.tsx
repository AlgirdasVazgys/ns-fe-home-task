import { render, screen } from '@testing-library/react';
import { FormError } from './FormError';

describe('FormError component', () => {
  it('should render value', () => {
    const testError = 'Test error';
    render(<FormError text={testError} />);
    const element = screen.getByText(testError);

    expect(element).toBeInTheDocument();
  });
});
