import { FC, InputHTMLAttributes } from 'react';
import { Input } from '@/components/ui/Input';

import './FormField.scss';

interface FormFieldProps extends Omit<InputHTMLAttributes<HTMLInputElement>, 'className'> {
  label: string;
  error?: string;
}

export const FormField: FC<FormFieldProps> = ({ label, error, ...inputProps }) => {
  return (
    <div className="FormField">
      <label className="FormField__label">
        <p className="FormField__label-text">{label}</p>
        <Input className="FormField__input" {...inputProps} />
        {error && <p className="FormField__error">{error}</p>}
      </label>
    </div>
  );
};
