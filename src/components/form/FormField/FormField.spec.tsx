import { render, screen } from '@testing-library/react';
import { FormField } from './FormField';

describe('FormField component', () => {
  it('should render label', () => {
    const testLabel = 'Test label';
    render(<FormField label={testLabel} />);
    const element = screen.getByText(testLabel);

    expect(element).toBeInTheDocument();
  });

  it('should render value', () => {
    const testValue = 'Test value';
    render(<FormField readOnly label="Label" value={testValue} />);
    const element = screen.getByDisplayValue(testValue);

    expect(element).toBeInTheDocument();
  });

  it('should render error', () => {
    const testError = 'Test error';
    render(<FormField readOnly label="Label" error={testError} />);
    const element = screen.getByText(testError);

    expect(element).toBeInTheDocument();
  });
});
