import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import { rootReducer, RootState } from '@/store/rootReducer';

export const configureStore = (preloadedState?: Partial<RootState>) => {
  return createStore(rootReducer, preloadedState, applyMiddleware(thunk));
};

export type AppStore = ReturnType<typeof configureStore>;
