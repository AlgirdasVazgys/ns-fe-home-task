import { initialState as initialAuthState } from '@/features/auth';
import { RootState } from '@/store/rootReducer';
import { storage } from '@/services/storage';
import { TOKEN_NAME } from '@/constants/metadata';

export const initialRootState: Partial<RootState> = {
  auth: {
    ...initialAuthState,
    isAuthenticated: Boolean(storage.getItem(TOKEN_NAME)),
  },
};
