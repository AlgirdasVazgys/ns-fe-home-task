import { AnyAction, combineReducers } from 'redux';
import { ThunkAction } from 'redux-thunk';
import { authReducer } from '@/features/auth';
import { serversReducer } from '@/features/servers';

export const rootReducer = combineReducers({
  auth: authReducer,
  servers: serversReducer,
});

export type RootState = ReturnType<typeof rootReducer>;
export type AppThunk<ReturnType = void> = ThunkAction<ReturnType, RootState, unknown, AnyAction>;
