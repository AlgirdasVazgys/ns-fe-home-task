import { validateForm } from '@/utils/validateForm';

describe('validateForm', () => {
  describe('when validation type is "required"', () => {
    it('should return errors and false if form is invalid', () => {
      const { isValid, errors } = validateForm({
        formData: {
          firstName: '',
          lastName: 'Swanson',
        },
        validations: {
          firstName: {
            required: {
              value: true,
              message: 'First name is required',
            },
          },
          lastName: {
            required: {
              value: true,
              message: 'Last name is required',
            },
          },
        },
      });
      expect(isValid).toBe(false);
      expect(errors).toEqual({ firstName: 'First name is required' });
    });

    it('should return no errors and true if form is valid', () => {
      const { isValid, errors } = validateForm({
        formData: {
          firstName: 'Ron',
          lastName: 'Swanson',
        },
        validations: {
          firstName: {
            required: {
              value: true,
              message: 'First name is required',
            },
          },
          lastName: {
            required: {
              value: true,
              message: 'Last name is required',
            },
          },
        },
      });
      expect(isValid).toBe(true);
      expect(errors).toEqual({});
    });
  });
});
