import { FormErrors, FormValidations } from '@/types/form';

interface ValidateFormProps<T extends Record<keyof T, unknown>> {
  validations?: FormValidations<T>;
  formData: T;
}

export const validateForm = <T extends Record<keyof T, unknown>>({
  validations,
  formData,
}: ValidateFormProps<T>) => {
  let isValid = true;
  const newErrors: FormErrors<T> = {};

  if (!validations) {
    return { isValid, errors: newErrors };
  }

  (Object.keys(validations) as Array<keyof T>).forEach((key) => {
    const value = formData[key];
    const validation = validations[key];

    if (validation?.required?.value && !value) {
      isValid = false;
      newErrors[key] = validation.required.message;
    }
  });

  return { errors: newErrors, isValid };
};
