import { ORDER } from '@/constants/misc';

interface SortParams<T extends Record<keyof T, unknown>> {
  list: T[];
  sortBy: keyof T;
  order: ORDER;
}

export const sortObjectsList = <T extends Record<keyof T, unknown>>({
  list,
  sortBy,
  order,
}: SortParams<T>) => {
  if (list.length === 0) {
    return list;
  }

  return [...list].sort((a, b) => {
    const aVal = a[sortBy];
    const bVal = b[sortBy];

    if (typeof aVal === 'number' && typeof bVal === 'number') {
      return order === ORDER.ASC ? aVal - bVal : bVal - aVal;
    }

    if (aVal < bVal) {
      return order === ORDER.ASC ? -1 : 1;
    }

    if (aVal > bVal) {
      return order === ORDER.ASC ? 1 : -1;
    }

    return 0;
  });
};
