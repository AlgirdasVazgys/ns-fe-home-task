import { sortObjectsList } from '@/utils/sortObjectsList';
import { ORDER } from '@/constants/misc';

describe('sortObjectsList', () => {
  describe('should return objects list sorted by', () => {
    const unsortedList = [
      { name: 'John', age: 42 },
      { name: 'Anne', age: 17 },
      { name: 'Zaira', age: 62 },
      { name: 'Carol', age: 39 },
    ];

    it('string in ascending order', () => {
      expect(
        sortObjectsList({
          list: unsortedList,
          sortBy: 'name',
          order: ORDER.ASC,
        }),
      ).toEqual([
        { name: 'Anne', age: 17 },
        { name: 'Carol', age: 39 },
        { name: 'John', age: 42 },
        { name: 'Zaira', age: 62 },
      ]);
    });

    it('string in descending order', () => {
      expect(
        sortObjectsList({
          list: unsortedList,
          sortBy: 'name',
          order: ORDER.DESC,
        }),
      ).toEqual([
        { name: 'Zaira', age: 62 },
        { name: 'John', age: 42 },
        { name: 'Carol', age: 39 },
        { name: 'Anne', age: 17 },
      ]);
    });

    it('number in ascending order', () => {
      expect(
        sortObjectsList({
          list: unsortedList,
          sortBy: 'age',
          order: ORDER.ASC,
        }),
      ).toEqual([
        { name: 'Anne', age: 17 },
        { name: 'Carol', age: 39 },
        { name: 'John', age: 42 },
        { name: 'Zaira', age: 62 },
      ]);
    });

    it('number in descending order', () => {
      expect(
        sortObjectsList({
          list: unsortedList,
          sortBy: 'age',
          order: ORDER.DESC,
        }),
      ).toEqual([
        { name: 'Zaira', age: 62 },
        { name: 'John', age: 42 },
        { name: 'Carol', age: 39 },
        { name: 'Anne', age: 17 },
      ]);
    });
  });
});
