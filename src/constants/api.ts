export const API_BASE_URL = 'https://playground.tesonet.lt/v1';
export enum API_ENDPOINT {
  TOKENS = '/tokens',
  SERVERS = '/servers',
}
