export enum ERROR {
  LOGIN = 'The username and password you entered did not match our records.',
  DEFAULT = 'Oops! Something went wrong!',
  REQUIRED_USERNAME = 'Please fill in your username',
  REQUIRED_PASSWORD = 'Please fill in your password',
}

export enum ORDER {
  ASC = 'ascending',
  DESC = 'descending',
}
