export const APP_IDENTIFIER = 'ns_servers';
export const APP_NAME = 'NS Servers';
export const TOKEN_NAME = `${APP_IDENTIFIER}_token`;
