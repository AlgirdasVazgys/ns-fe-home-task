import { Store } from 'redux';
import { storage } from '@/services/storage';
import { TOKEN_NAME } from '@/constants/metadata';
import { API_ENDPOINT } from '@/constants/api';
import { apiClient } from '@/services/apiClient';
import { AUTH } from '@/features/auth';

export const setupInterceptors = (store: Store) => {
  apiClient.interceptors.request.use(
    function (config) {
      const token = storage.getItem(TOKEN_NAME);

      if (token) {
        config.headers = {
          ...config.headers,
          Authorization: `Bearer ${token}`,
        };
      }

      return config;
    },
    function (error) {
      return Promise.reject(error);
    },
  );

  apiClient.interceptors.response.use(
    function (response) {
      return response;
    },
    function (error) {
      if (error.config.url !== API_ENDPOINT.TOKENS && error.response?.status === 401) {
        store.dispatch({ type: AUTH.LOGOUT });
      }

      return Promise.reject(error);
    },
  );
};
