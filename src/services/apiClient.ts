import axios from 'axios';
import { API_BASE_URL } from '@/constants/api';

const DEFAULT_HEADERS = {
  'Content-Type': 'application/json',
};

export const apiClient = axios.create({
  baseURL: API_BASE_URL,
  headers: {
    ...DEFAULT_HEADERS,
  },
});
