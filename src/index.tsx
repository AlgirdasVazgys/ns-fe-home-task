import 'react-app-polyfill/ie11';
import 'core-js/stable';
import { createRoot } from 'react-dom/client';
import { App } from './App';
import './styles/init.scss';

const rootElement = document.getElementById('root');
const root = createRoot(rootElement as HTMLElement);

root.render(<App />);
