# NS Fe Home Task

## Intro
The decisions for the project structure/architecture/technologies/etc were made considering this as a production ready, large scale application and to meet task requirements. Libraries (especially UI) were avoided as much as possible as per task requirements and replaced with custom reusable hooks/custom components or generic ones (i.e. redux+redux-thunk).

## Getting started

`npm install` install dependencies

`npm start` run app in development mode

`npm run build` create production build

`npm test` run tests

## Technologies

### Project config
- `Typescript` for types (task requirement)
- `eslint + prettier` to ensure consistent code formatting
- `husky` for handling pre-commit hooks. Code is run through linter before each commit to ensure that formatting errors do not go into repo.
- `Jest + React Testing Library` for tests
- `Webpack` optimized app bundle generation (task requirement)
- `PostCSS + plugins` to handle styling prefixes generation for different browsers and styling code minimization

### Code related
- `redux + react-redux + redux-thunk` state management (task requirement)
- `axios` handling api requests
- `BEM methodology + scss` for styling

## Project structure

`src` folder structure:

```sh
src
|
+-- __mocks__         # library mocks
|
+-- assets            # contains files such as images, fonts, etc.
|
+-- components        # shared components used across the entire application
|
+-- constants         # global constants such as token name, route names, endpoint names etc.
|
+-- features          # feature based modules
|
+-- hooks             # shared hooks used across the entire application
|
+-- routes            # routes configuration
|
+-- services          # data layer helpers such as wrapped api client, api interceptors setup, etc.
|
+-- store             # configuration of root store, root reducer etc.
|
+-- test              # test setup and utilities
|
+-- types             # base types used across the application
|
+-- utils             # shared pure utility functions
```
### Feature structure

To ensure efficient and maintainable scalability of the app, feature should contain all feature specific code. This approach helps to avoid bloating of shared folders which content grows linear with scale of the application.

Example feature:

```sh
src/features/example-feature
|
+-- api         # feature related api requests declarations
|
+-- components  # feature specific components
|
+-- hooks       # feature specific hooks
|
+-- routes      # route components for feature pages
|
+-- store       # feature specific reducers and actions
|
+-- types       # feature related typescript types
|
+-- utils       # feature specific pure utility functions
|
+-- index.ts    # public api of the feature, exports everything what is used outside of the feature
```
### Component structure.

Folders with components collections (`src/features/example-feature/components/` and `src/components/component-type/`) should ideally be kept flat. Descriptive names of components are favored over nesting.

Example component:
```sh
src/features/example-feature/components/ExampleComponent
|
+-- ExampleComponent.spec.tsx  # component tests
|
+-- ExampleComponent.scss      # component styles
|
+-- ExampleComponent.tsx       # component code
|
+-- index.ts                   # component exports
```

## Future improvements

- Improve design and ux (page transitions, visual appearance, etc.)
- Improve handling of login form errors (clear errors on 'touched' state, etc.)
- Consider structure improvements with consolidating shared components with the companion hooks and types (i.e. store useTable together with Table and write tests for both of them at once, which might serve as a documentation for its usage)
- Create common sizes variables (element heights, text heights, paddings, etc.)
- Improving authentication/redirects handling
- Split routes separately into public and private in `routes` folder
- Adding more documentation/improving unit tests to better serve as info on inner workings of components/hooks 